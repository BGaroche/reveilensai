package com.example.reveil_titan_bruno;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class EditActivity extends AppCompatActivity {

    private EditText hcInput;
    private EditText trajetInput;
    private EditText prepaInput;
    private SharedPreferences prefs;
    private Button arret_bus;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        prefs = getSharedPreferences("MY_DATA2", MODE_PRIVATE);
        String hc = prefs.getString("HEURE_COURS", "09:45");
        String trajet = prefs.getString("TEMPS_TRAJET", "00:45");
        String prepa = prefs.getString("TEMPS_PREPARATION", "01:00");
        arret_bus = (Button) findViewById(R.id.arret_bus);
        arret_bus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(this, Arret_bus.class);
                startActivity(intent);
            }
        });

        downloadJSON("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-topologie-dessertes-td&facet=libellecourtparcours&facet=nomcourtligne&facet=nomarret&facet=estmonteeautorisee&facet=estdescenteautorisee&refine.nomcourtligne=57&apikey=79c327454a97a8faa33d7b06f3d86e364da49e9c8766a0e10439ee47");


        hcInput = (EditText)findViewById(R.id.hcIn);
        trajetInput = (EditText)findViewById(R.id.trajetIn);
        prepaInput = (EditText)findViewById(R.id.prepaIn);



        // Set default value.
        hcInput.setText(hc);
        trajetInput.setText(trajet);
        prepaInput.setText(prepa);
        /*
        hcInput.setHint(prefs.getString("HEURE_COURS", "09:45"));
        trajetInput.setHint(prefs.getString("TEMPS_TRAJET", "00:45"));
        prepaInput.setHint(prefs.getString("TEMPS_PREPARATION", "01:00"));
        */



        // Change Value
        hcInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(EditActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        if (minutes<10) {
                            if (hourOfDay < 10) {
                                hcInput.setText("0" + hourOfDay + ":" + "0"+minutes);
                            } else {
                                hcInput.setText(hourOfDay + ":" + "0"+minutes);
                            }
                        } else {
                            if (hourOfDay < 10) {
                                hcInput.setText("0" + hourOfDay + ":" + minutes);
                            } else {
                                hcInput.setText(hourOfDay + ":" + minutes);
                            }

                        }
                    }
                }, 9, 45, true);

                timePickerDialog.show();
            }
        });

        trajetInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(EditActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        if (minutes<10) {
                            if (hourOfDay < 10) {
                                trajetInput.setText("0" + hourOfDay + ":" +  "0"+minutes);
                            } else {
                                trajetInput.setText(hourOfDay + ":" +  "0"+minutes);
                            }
                        } else {
                            if (hourOfDay < 10) {
                                trajetInput.setText("0" + hourOfDay + ":" + minutes);
                            } else {
                                trajetInput.setText(hourOfDay + ":" + minutes);
                            }

                        }
                    }
                }, 0, 30, true);

                timePickerDialog.show();
            }
        });

        prepaInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(EditActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        if (minutes<10) {
                            if (hourOfDay < 10) {
                                prepaInput.setText("0" + hourOfDay + ":" + "0"+minutes);
                            } else {
                                prepaInput.setText(hourOfDay + ":" + "0"+minutes);
                            }
                        } else {
                            if (hourOfDay < 10) {
                                prepaInput.setText("0" + hourOfDay + ":" + minutes);
                            } else {
                                prepaInput.setText(hourOfDay + ":" + minutes);
                            }

                        }
                    }
                }, 1, 0, true);

                timePickerDialog.show();
            }
        });



    }


    private boolean validateTime(EditText editt) {
        Time24HoursValidator time24HoursValidator = new Time24HoursValidator();
        String val = editt.getText().toString();

        if (val.isEmpty()) {
            editt.setError("Le champ ne peut être vide");
            return false;
        } else if (!time24HoursValidator.validate(val)) {
            editt.setError("Entrer une heure au format hh:mm");
            return false;
        } else {
            editt.setError(null);
            return true;
        }
    }


    private String DiffTime (String Ta, String Tb)  {
        String result;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date TaDate = sdf.parse(Ta);
            Date TbDate = sdf.parse(Tb);
            long difference = TaDate.getTime() - TbDate.getTime();
            if (difference < 0) {
                Date dateMax = sdf.parse("24:00");
                Date dateMin = sdf.parse("00:00");
                difference = (dateMax.getTime() - TbDate.getTime()) + (TaDate.getTime() - dateMin.getTime());
            }

            int days = (int) (difference / (1000 * 60 * 60 * 24));
            int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
            int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
            Log.i("log_tag", "Hours: " + hours + ", Mins: " + min);
            if (min<10) {
                if (hours < 10) {
                    result = "0"+String.valueOf(hours)+":"+"0"+String.valueOf(min);
                } else {
                    result = String.valueOf(hours)+":"+"0"+String.valueOf(min);
                }
            } else {
                if (hours < 10) {
                    result = "0"+String.valueOf(hours)+":"+String.valueOf(min);
                } else {
                    result = String.valueOf(hours)+":"+String.valueOf(min);
                }

            }
            return result;

        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(EditActivity.this,"pb",Toast.LENGTH_SHORT).show();
            return "00:07";

        }

    }




    public void saveData(View view) throws ParseException {
             // Get input text.
        String hc = hcInput.getText().toString();
        String trajet = trajetInput.getText().toString();
        String prepa = prepaInput.getText().toString();
        String depart = DiffTime(hc,trajet);
        String reveil = DiffTime(depart,prepa);

        if (!validateTime(hcInput)|!validateTime(trajetInput)|!validateTime(prepaInput)) {
            return;
        }


        // Save data.
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("HEURE_COURS", hc);
        editor.putString("TEMPS_TRAJET",trajet);
        editor.putString("TEMPS_PREPARATION", prepa);
        editor.putString("HEURE_DEPART",depart);
        editor.putString("HEURE_REVEIL", reveil);

        editor.apply();

        String input = "Heures de cours : " + hc;
        input += "\n";
        input += "Temps de trajet : " + trajet ;
        input += "\n";
        input += "Temps de préparation : " + prepa;

        Toast.makeText(EditActivity.this,"Vos paramètres sont sauvegardées",Toast.LENGTH_SHORT).show();
        Toast.makeText(this, input, Toast.LENGTH_LONG).show();


        // Return to main activity.
        startActivity(new Intent(getApplicationContext(), MainActivity.class));

    }

    private void loadIntoListView(String json) throws JSONException, IOException {
        ObjectMapper mapper= new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
        List_arret bus = mapper.readValue(json, List_arret.class);
        String[] stocks = new String[bus.getRecord_arrets().size()];
        for (int i = 0; i < bus.getRecord_arrets().size(); i++) {
            Record_arret records = bus.getRecord_arrets().get(i);
            stocks[i] = ""+records.getFields_arret().getNomarret();
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stocks);
        listView.setAdapter(arrayAdapter);
    }

    private void downloadJSON(final String urlWebService) {



        class DownloadJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                try {
                    loadIntoListView(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... voids) {
                try {
                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json + "\n");
                    }
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }
            }
        }


    }
}





