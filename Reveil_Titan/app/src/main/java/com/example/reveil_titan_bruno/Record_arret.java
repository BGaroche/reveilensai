package com.example.reveil_titan_bruno;

public class Record_arret {
    private  String datasetid;
    private String recordid;
    private Fields_arret Fields_arret;

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setFields_arret(com.example.reveil_titan_bruno.Fields_arret fields_arret) {
        Fields_arret = fields_arret;
    }

    public com.example.reveil_titan_bruno.Fields_arret getFields_arret() {
        return Fields_arret;
    }

    public void setDatasetid(String datasetid) {
        this.datasetid = datasetid;
    }

    public String getDatasetid() {
        return datasetid;
    }


}
