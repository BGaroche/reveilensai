package com.example.reveil_titan_bruno;

public class Fields_arret {
    private String nomcourtligne;
    private String nomarret;
    private String idarret;

    public String getNomcourtligne() {
        return nomcourtligne;
    }

    public void setNomcourtligne(String nomcourtligne) {
        this.nomcourtligne = nomcourtligne;
    }

    public String getNomarret() {
        return nomarret;
    }

    public void setNomarret(String nomarret) {
        this.nomarret = nomarret;
    }

    public String getIdarret() {
        return idarret;
    }

    public void setIdarret(String idarret) {
        this.idarret = idarret;
    }
}