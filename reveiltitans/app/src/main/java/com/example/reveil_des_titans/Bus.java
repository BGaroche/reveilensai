package com.example.reveil_des_titans;

import java.util.List;

public class Bus {
    private int nhits;
    private List<Records> records;

    private void setNhits(int nhits) {this.nhits=nhits;
    }

    public int getNhits(){return nhits;}


    public void setRecords(List<Records> records) {
        this.records = records;
    }

    public List<Records> getRecords() {
        return records;
    }
}
