package com.example.reveil_des_titans;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private Button preference;
    private Bus bus1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.preference = findViewById(R.id.preference);
        preference.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent otheractivity = new Intent(getApplicationContext(),Screen_reglages.class);
                startActivity(otheractivity);
                Thread tab_star = new Thread(){

                    public void run(){
                        try{
                            SharedPreferences prefs = getSharedPreferences("MY_DATA2", MODE_PRIVATE);
                            URL url = new URL("https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-bus-circulation-passages-tr&sort=nomcourtligne&facet=idligne&facet=nomcourtligne&facet=sens&facet=destination&facet=precision&refine.idligne=0057&apikey=79c327454a97a8faa33d7b06f3d86e364da49e9c8766a0e10439ee47");
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            ObjectMapper mapper= new ObjectMapper();
                            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
                            InputStream st2 = connection.getInputStream();
                            Bus bus1 = mapper.readValue(st2, Bus.class);
                            Log.i("Screen_reglages","nhits"+bus1.getNhits()+bus1.getRecords().get(0).getDatasetid()+bus1.getRecords().size());


                            /* POUR RECUPERER LE TRUC FAIRE String maString = prefs.getString("NHITS", "0");*/
                        }  catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally{
                            Log.i("Screen_reglages","final");
                        }
                    }
                };
                tab_star.start();
           }
          }

        );

    }
}
