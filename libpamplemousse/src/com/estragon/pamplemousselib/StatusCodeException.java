package com.estragon.pamplemousselib;

public class StatusCodeException extends Exception {
	private int code = 200;
	public StatusCodeException(int code) {
		this.code = code;
	}
	
	public int getErrorCode() {
		return code;
	}
}
