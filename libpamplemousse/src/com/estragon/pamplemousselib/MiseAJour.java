package com.estragon.pamplemousselib;


public class MiseAJour implements IRequete {

	public static final int ETAPE_CONNEXION = 0, ETAPE_TOKEN = 1, ETAPE_EMPLOI_DU_TEMPS = 2;
	int etape = 0;
	MAJListener listener;
	String pseudo;
	String mdp;
	RequeteEmploiDuTemps requeteEmploiDuTemps;
	

	public MiseAJour(MAJListener listener,String pseudo,String mdp) {
		this.listener = listener;
		this.pseudo = pseudo;
		this.mdp = mdp;
	}

	public void executer() {
		Requete.cookieStore.clear();
		Requete.LT = "";
		new RequeteConnexion(this).executer();
	}
	
	public String getResultat() {
		if (requeteEmploiDuTemps == null) {
			return null;
		}
		return requeteEmploiDuTemps.getResultat();
	}

	public void etapeSuivante() {
		etape++;	
		listener.nouvelleEtape(etape);
		switch (etape) {
		case	ETAPE_CONNEXION:
			//Jamais exécuté en théorie
			new RequeteConnexion(this).executer();
			break;
		case	ETAPE_TOKEN:
			new RequeteToken(this,pseudo,mdp).executer();
			break;
		case	ETAPE_EMPLOI_DU_TEMPS:
			requeteEmploiDuTemps = new RequeteEmploiDuTemps( this);
			requeteEmploiDuTemps.executer();
			break;
			
		}
	}


	public interface MAJListener {
		public void nouvelleEtape(int etape);
		public void echec();
	}


	@Override
	public void onAbort() {
		// TODO Auto-generated method stub
		listener.echec();
	}

	@Override
	public void onSuccess(String data) {
		// TODO Auto-generated method stub
		etapeSuivante();
	}

	@Override
	public void onErreur(Exception erreur) {
		// TODO Auto-generated method stub
		listener.echec();
	}
}
