package com.estragon.pamplemousselib;

import org.json.me.JSONObject;

import com.estragon.pamplemousselib.MiseAJour.MAJListener;

public class Main {

	
	public static void main(String[] args) {
		chargerEmploiDuTemps("","");
	}
	
	public static String chargerEmploiDuTemps(String login, String mdp) {
		SousMain sousMain = new SousMain(login,mdp);
		sousMain.lancer();
		String result = sousMain.getResultat();
		if (result == null) {
			JSONObject object = new JSONObject();
			try {
				object.put("error", "error");
				return object.toString(1);
			}
			catch (Exception e) {
				return "";
			}
		}
		return result;
	}
	
	public static class SousMain implements MAJListener  {
		
		String login, mdp;
		MiseAJour maj;
		
		public SousMain(String login, String mdp) {
			this.login = login;
			this.mdp = mdp;
		}
		
		public void lancer() {
			maj = new MiseAJour(this,login,mdp);
			maj.executer();
		}
		
		public String getResultat() {
			return maj.getResultat();
		}

		@Override
		public void nouvelleEtape(int etape) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void echec() {
			// TODO Auto-generated method stub
			
		}
	}
}
