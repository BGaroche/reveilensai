package com.example.reveil_titan_bruno;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.widget.Toast;


public class ReveilReceiver extends BroadcastReceiver {
    MediaPlayer mp;
    @Override
    public void onReceive(Context context, Intent intent) {

        Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(3000);

        mp=MediaPlayer.create(context, R.raw.alrm2 );
        mp.start();
        Toast.makeText(context, "Debout c'est l'heure d'aller à l'Ensai", Toast.LENGTH_LONG).show();
    }

}

