package com.example.reveil_titan_bruno;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.widget.Toast;


public class DepartReceiver extends BroadcastReceiver {
    
    MediaPlayer mp;
    @Override
    public void onReceive(Context context, Intent intent) {

        Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(3000);

        mp=MediaPlayer.create(context, R.raw.alrm3 );
        mp.start();
        Toast.makeText(context, "Dehors, c'est l'heure du départ", Toast.LENGTH_LONG).show();
        Toast.makeText(context, "Si tu te dépeches, tu as le temps d'acheter un pain typiquement japonais", Toast.LENGTH_LONG).show();
    }

}

