package com.example.reveil_titan_bruno;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    AlarmManager alarmManagerOne;
    AlarmManager alarmManagerTwo;
    private PendingIntent pendingIntentOne;
    private PendingIntent pendingIntentTwo;
    private static MainActivity inst;

    /*public static MainActivity instance() {
        return inst;
    }*/

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences prefs = getSharedPreferences("MY_DATA2", MODE_PRIVATE);

        // getXXX(key, default value)
        String hc = prefs.getString("HEURE_COURS", "09:45");
        String depart =prefs.getString("HEURE_DEPART", "09:05");
        String reveil=prefs.getString("HEURE_REVEIL", "08:05");


        // Set values
        ((TextView)findViewById(R.id.hcLabel)).setText(hc);
        ((TextView)findViewById(R.id.departLabel)).setText(depart);
        ((TextView)findViewById(R.id.reveilLabel)).setText(reveil);
        ToggleButton reveilToggle = (ToggleButton) findViewById(R.id.reveilOn);
        alarmManagerOne = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManagerTwo = (AlarmManager) getSystemService(ALARM_SERVICE);
    }

    public void onToggleClicked(View view) {
        if (((ToggleButton) view).isChecked()) {
            Toast.makeText(this, "Demain tu seras à l'heure", Toast.LENGTH_LONG).show();
            SharedPreferences prefs = getSharedPreferences("MY_DATA2", MODE_PRIVATE);
            String reveil=prefs.getString("HEURE_REVEIL", "08:05");
            String depart=prefs.getString("HEURE_DEPART", "08:05");

            Log.d("MyActivity", "Reveil On");
            Calendar heure_actuel = Calendar.getInstance();
            Calendar heure_reveil = Calendar.getInstance();
            Calendar heure_depart = Calendar.getInstance();


            Date date = new Date();
            heure_actuel.setTime(date);
            heure_reveil.setTime(date);
            heure_depart.setTime(date);

            int hoursReveil = Integer.parseInt(reveil.substring(0, 2));
            int minutesReveil = Integer.parseInt(reveil.substring(3, 5));
            heure_reveil.set(Calendar.HOUR_OF_DAY, hoursReveil);
            heure_reveil.set(Calendar.MINUTE, minutesReveil);
            heure_reveil.set(Calendar.SECOND, 0);

            int hoursDepart = Integer.parseInt(depart.substring(0, 2));
            int minutesDepart = Integer.parseInt(depart.substring(3, 5));
            heure_depart.set(Calendar.HOUR_OF_DAY, hoursDepart);
            heure_depart.set(Calendar.MINUTE, minutesDepart);
            heure_depart.set(Calendar.SECOND, 0);



            if (heure_reveil.before(heure_actuel)){
                heure_reveil.add(Calendar.DATE,1);
            }
            if (heure_depart.before(heure_actuel)){
                heure_depart.add(Calendar.DATE,1);
            }


            Intent myIntentOne = new Intent(MainActivity.this, ReveilReceiver.class);
            pendingIntentOne = PendingIntent.getBroadcast(MainActivity.this, 0, myIntentOne, 0);
            alarmManagerOne.set(AlarmManager.RTC_WAKEUP, heure_reveil.getTimeInMillis(), pendingIntentOne);

            Intent myIntentTwo = new Intent(MainActivity.this, DepartReceiver.class);
            pendingIntentTwo = PendingIntent.getBroadcast(MainActivity.this, 0, myIntentTwo, 0);
            alarmManagerTwo.set(AlarmManager.RTC_WAKEUP, heure_depart.getTimeInMillis(), pendingIntentTwo);

        } else {
            alarmManagerOne.cancel(pendingIntentOne);
            alarmManagerTwo.cancel(pendingIntentTwo);

            Log.d("MyActivity", "Alarm Off");
        }
    }



    public void showEdit(View view) {
        startActivity(new Intent(getApplicationContext(), EditActivity.class));
    }
}